var path = require('path');
var express = require('express');
var port = 8080;

var app = express();

var staticPath = path.join(__dirname);
app.use(express.static(staticPath));

app.listen(port, function() {
    console.log('listening on port: ' + port);
});