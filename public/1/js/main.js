var content = document.querySelector('.main-content');

var x = ['a','s','d','a','a','s','s'];

var filter = function(arr){
    return arr.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    });
};

var createItems = function(item, content){
    var node = document.createElement("LI");
    var textnode = document.createTextNode(item);
    node.className = "foo";
    node.appendChild(textnode);
    content.appendChild(node);
};

var reduceArray = filter(x);

for(var i in reduceArray) {
    createItems(reduceArray[i], content);
}